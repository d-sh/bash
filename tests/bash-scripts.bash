#!/usr/bin/env bash

# load scripts from template
scripts=/tmp/bash-scripts.sh
echo "#!/bin/bash" > $scripts
sed -n '/BEGSCRIPT/,/ENDSCRIPT/p' "$(dirname ${BASH_SOURCE[0]})/../templates/gitlab-ci-bash.yml" >> $scripts
source $scripts
