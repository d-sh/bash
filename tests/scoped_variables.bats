#!/usr/bin/env bats
load "$BATS_LIBRARIES_DIR/bats-support/load.bash"
load "$BATS_LIBRARIES_DIR/bats-assert/load.bash"
load 'bash-scripts'


function setup() {
  export TESTVAR="default"
  export scoped__TESTVAR__if__CONDVAR__equals__case1="case1"
  export scoped__TESTVAR__if__CONDVAR__startswith__case2="case2"
  export scoped__TESTVAR__if__CONDVAR__endswith__case3="case3"
  export scoped__TESTVAR__if__CONDVAR__contains__case4="case4"
  export scoped__TESTVAR__if__CONDVAR__in__apple__banana__orange="fruit"
}

function unscope_variables_and_check() {
  unscope_variables
  echo ">>> TESTVAR=$TESTVAR"
}
 

@test "no scoped var" {
  # GIVEN

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=default"
}

@test "equals condition should match" {
  # GIVEN
  export CONDVAR=case1

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=case1"
}

@test "startswith condition should match" {
  # GIVEN
  export CONDVAR='case2 and bla bla'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=case2"
}

@test "startswith condition should not match" {
  # GIVEN
  export CONDVAR='does not start with case2'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=default"
}

@test "endswith condition should match" {
  # GIVEN
  export CONDVAR='ends with case3'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=case3"
}

@test "endswith condition should not match" {
  # GIVEN
  export CONDVAR='does not end with case3...'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=default"
}

@test "contains condition should match" {
  # GIVEN
  export CONDVAR='contains case4 in the middle'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=case4"
}

@test "in condition should match 1" {
  # GIVEN
  export CONDVAR='apple'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=fruit"
}

@test "in condition should match 2" {
  # GIVEN
  export CONDVAR='banana'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=fruit"
}

@test "in condition should match 3" {
  # GIVEN
  export CONDVAR='orange'

  # WHEN
  run unscope_variables_and_check

  # THEN
  assert_line ">>> TESTVAR=fruit"
}
